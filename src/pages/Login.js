import {Form , Button, Row, Col} from 'react-bootstrap';
import { Fragment } from 'react';
import {useState, useEffect, useContext } from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';

export default function Login(){

    // Login useState

    const [email , setEmail] = useState('');
    const [password , setPassword] = useState('');

    const navigate = useNavigate()

    // const[user, setUser] = useState(localStorage.getItem('email'));

    //Allows us to consume the UserContext object and its's properties for user validation;

    const {user, setUser} = useContext(UserContext);

    console.log(user);

    
       

    // Button useState
    const [isActive , setIsActive] = useState(false)

    useEffect(()=>{
        if(email !== "" && password !== ""){
            setIsActive(true)
        }else{
            setIsActive(false)
        }

    },[email,password])




    function login(event){
        event.preventDefault();

        //if you want to add the email of the authenticated user in the local storage
            //Syntax:
                //localStorage.setItem("propertName", value)

  
        /*Process a fetch request to corresponding backend API*/
        //Syntax: fetch('ulr', {options});

        fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(result => result.json())
        .then(data => {
            console.log(data)

            if(data === false){
                Swal.fire({
                    title: "Authentication failed!",
                    icon: "error",
                    text: "Please try again!"
                })
            }else{
                localStorage.setItem('token', data.auth);
                retrieveUserDetails(localStorage.getItem('token'));

                Swal.fire({
                    title: "Authentication successful!",
                    icon: 'success',
                    text: "Welcome to Zuitt!"
                })

                navigate('/');
            }
        })



        /*localStorage.setItem("email", email);
        console.log(localStorage.getItem("email"));
        setUser(localStorage.getItem("email"));
        alert("You are now logged in")
        setEmail('');
        setPassword('');*/

        

        
        // useNavigate('/')

       /* navigate("/")*/
    }

    const retrieveUserDetails = (token) => {

        //the token sent as part of the request's header information


        fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
            headers:{
                Authorization: `Bearer ${token}`
            }
        })
        .then(result => result.json())
        .then(data => {
            console.log(data)

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }




    return(
        user ?
        <Navigate to = "/*"/>
        :
        <Row className = "mt-5">
            <Col className = 'col-md-6 col-10 mx-auto bg-success p-3'>
                <Fragment>
                <h1 className='text-center'>Login</h1>
                <Form className='mt-5' onSubmit={event => login(event)}>
                  <Form.Group className="mb-3" controlId="formGroupEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value = {email}
                    onChange ={event => setEmail(event.target.value)} 
                    required

                    />
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="formGroupPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value = {password}
                    onChange ={event => setPassword(event.target.value)} 
                    required />
                  </Form.Group >
                        {
                            isActive?
                            <Button variant="primary" type="submit" className = "mx-auto">
                            Submit
                             </Button>
                             :
                             <Button variant="dark" type="submit" disabled>
                            Submit
                             </Button>
                        }
                    </Form>
                    </Fragment>
            </Col>
        </Row>
    )
}
